# OpenERP v7.0 - Wolisso #

### What is this repository for? ###

* Set of modules for custom OpenERP v7.0 used for ISF/CUAMM in St.Luke Hospital (Wolisso - Ethiopia)
* Custom OpenERP v7.0 on the related [repository](https://bitbucket.org/mwithi/openerp-7.0)
* Custom CIMS set of modules on the related [repository](https://bitbucket.org/mwithi/cims)

### How do I get set up? ###

#### Requirements ####

* Same requirements as for custom OpenERP v7.0 on related [repository](https://bitbucket.org/mwithi/openerp-7.0)

#### Istructions ####

* clone branch openeerp7 on [this other](https://bitbucket.org/mwithi/openerp-7.0) repository and make it working
* clone branch master on [this other](https://bitbucket.org/mwithi/cims) and add root folder in addons_path in openerp-server.conf
* fork this repo and add root folder in addons_path in openerp-server.conf
* restart openerp-server.py
* You can restore a Wolisso dump (if you have any) or start from an EMPTY DB (see [TODO: OpenERP Manual](https://bitbucket.org/mwithi/cims-wolisso/downloads/OpenERPManual.pdf)):
    * create new DB
    * go to Settings -> Update Module List
    * go to Settings -> Installed Module 
    * install isf_wolisso_coa
    * install isf_wolisso_data
    * install isf_wolisso_*
    * ...


### Contribution guidelines ###

* Fork, develop & pull-request
* Double analyze the existing code, don't write what is already written, be DRY (Don't Repeat Yourself)
* Dumb code is better than clever one when is time to share
* Write comments when only "YOU" know what you are doing
* Optimize only after achieved
* Less is more!

### Who do I talk to? ###

* Repo owner or admin: [mwithi](https://github.com/mwithi)
* Other community or team contact: [ISF - Informatici Senza Frontiere ONLUS](http://www.informaticisenzafrontiere.org/)