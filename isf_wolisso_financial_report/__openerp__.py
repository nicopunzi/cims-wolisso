# -*- coding: utf-8 -*-

{
	'name': 'ISF St.Luke Hospital of Wolisso Financial Report',
	'version': '1.0',
	'category': 'Tools',
	'description': """

ISF St.Luke Hospital of Wolisso Financial Report 
================================================

Add the Financial Statement Report for St.Luke Hospital of Wolisso:

""",
	'author': 'Alessandro Domanico (alessandro.domanico@informaticisenzafrontiere.org)',
	'website': 'www.informaticisenzafrontiere.org',
	'license': 'AGPL-3',
	'depends': ['isf_cims_module',
			'isf_wolisso_coa',
			'isf_wolisso_data',
			'isf_account_financial_report'],
	'data': [
            'data/account.financial.report.csv',
            'isf_financial_report.xml',
    	],
	'demo': [],
	'installable' : True,
}

